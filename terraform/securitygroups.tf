resource "aws_security_group" "vespene_group" {
  name        = "vespene_group"
  description = "Allows ssh, http, port 8000 and PostgreSQL port"
  vpc_id      = "${var.VpcId}"

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = "${var.AllowedIps}"
  }
  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["${var.AllowedIps}"]
  }
  ingress {
    protocol    = "tcp"
    from_port   = 5432
    to_port     = 5432
    cidr_blocks = ["${var.AllowedIps}"]
  }
  ingress {
    protocol    = "tcp"
    from_port   = 8000
    to_port     = 8000
    cidr_blocks = ["${var.AllowedIps}"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name    = "${var.prefix}_group"
  }
}
