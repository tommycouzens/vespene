resource "aws_instance" "vespene" {
  ami                         = "${lookup(var.Amazon2AMIs, var.region)}"
  subnet_id                   = "${var.SubnetId}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.KeyName}"
  vpc_security_group_ids      = ["${aws_security_group.vespene_group.id}"]
  associate_public_ip_address = true
  user_data                   = "${var.user_data}"

  # Copies the vespene folder to ~/vespene, saves having to git clone
  provisioner "file" {
    source      = "../vespene"
    destination = "~"
  }

  # makes vespene able to sudo
  provisioner "file" {
    source      = "files/90-vespene"
    destination = "/etc/sudoers.d/90-vespene"
  }

  connection {
  user         = "ec2-user"
  private_key  = "${file("~/.aws/TommyNV.pem")}"
  }

  # Run the commands to start vespene
  provisioner "remote-exec" {
    inline = [
      "cd ~/vespene/setup",
      "bash 1_prepare.sh",
      "bash ~/vespene/setup/2_database.sh",
      "bash ~/vespene/setup/3_application.sh",
      "printf 'ec2-user\ntommy@automationlogic.com\nvespene\nvespene\n' | bash 4_superuser.sh",
      "bash 5_tutorial.sh",
      "bash 6_services.sh"
    ]
  }

  tags {
    Name = "${var.prefix}"
  }
}

variable "user_data" {
  description = "Instance user data. Do not pass gzip-compressed data via this argument"
  default     = <<HEREDOC
  #!/bin/bash
  yum -y install git
HEREDOC
}


output "VespeneIp" {
  value = "${aws_instance.vespene.public_ip}"
}
