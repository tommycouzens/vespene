variable "access_key" {}
variable "secret_key" {}
variable "region" {
  default = "us-east-1"
}

provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}
variable "prefix" {
  default = "vespene"
}

variable "KeyName" {
  default = "TommyNV"
}

variable "Amazon2AMIs" {
  type = "map"
  default = {
    "eu-west-2" = "ami-0274e11dced17bb5b"
    "us-east-1" = "ami-009d6802948d06e52"
  }
}

variable "UbuntuAMI" {
  default = "ami-0ac019f4fcb7cb7e6"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "AZs" {
  description = "Availability zones"
  type = "list"
  default = ["us-east-1a", "us-east-1b", "us-east-1c"]
}

variable "VespeneSecurityGroup" {
  default = "sg-02fc0788a3f73e610"
}

variable "VpcId" {
  default = "vpc-eb563690"
}

variable "OfficeIp" {
  default = "77.108.144.180/32"
}

# Second one is the wittering Ip and can be deleted
variable "AllowedIps" {
  type = "list"
  default = ["77.108.144.180/32", "86.187.166.199/32"]
}

variable "SubnetId" {
  default = "subnet-01bee13857f2290de"
}

variable "SubnetId2" {
  default = "subnet-09ce22752042c852f"
}

variable "DBNAME" {
  default = "vespene"
}

variable "DBUSER" {
  default = "vespene"
}

variable "DBPASS" {
  default = "vespene!"
}
